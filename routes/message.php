<?php 
  // all routes will be called inside Server class
  // so a $this will be avavilale containing relevant info
   
  $this->add_route("/", "GET", "message/index.php");
  $this->add_route("/profile", "GET", "message/profile.php");

  $this->not_found();