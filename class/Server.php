<?php
  class Server {
    private $route_basepath;
    public $not_found_script;
    public $method;
    public $route;
    public $data = array("get" => array(), "post" => array());
    public $error = false;
    public $route_whole;
    public $route_current;

    function __construct($route_basepath = "routes") {
      $this->route_basepath = $route_basepath;
      $this->method = $_SERVER["REQUEST_METHOD"];
      $route = explode("?", $_SERVER["REQUEST_URI"])[0];
      $this->route_whole = "/". trim($route, "/");
      $this->route = explode("/", trim($route, "/"));
 
      // set GET data
      foreach($_GET as $key=>$value) {
        $this->data["get"][$key] = $value;
      }
      $_GET = "";
      
      // set POST/BODY data
      $post_datas = array();
      if ( ($raw_post = file_get_contents("php://input")) !== "" && !($post_datas = json_decode($raw_post, JSON_UNESCAPED_SLASHES)) ) {
        $this->error = "Invalid JSON given";
        return;
      }
      foreach($post_datas as $key=>$value) {
        $this->data["post"][$key] = $value;
      }
    }

    function add_route($location, $method, $script, $should_match_exact = false) {
      $match_found = false;
      $location = trim($location, "/");

      if (!$should_match_exact && ((array_search($location, $this->route) === 0) || ($location == "" && count($this->route) == 0))) {
        $match_found = true;
        $this->route_current = array_splice($this->route, 0, 1);
      } else if ($should_match_exact && trim($this->route_whole, "/") === $location) {
        $match_found = true;
      }

      if ($match_found) {
        require_once(__DIR__."/../{$this->route_basepath}/$script");

        // method should exit after called
        // this line should not be displayed
        echo "final request not send";
        exit();
      }
    }

    function get_headers() {
      return $_SERVER;
    }

    function not_found() {
      require_once(__DIR__."/../{$this->route_basepath}/{$this->not_found_script}");
    }
  }