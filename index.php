<?php
  require_once(__DIR__ . "/class/Server.php");
  require_once(__DIR__ . "/class/Response.php");

  $server = new Server();
  $res = new Response();

  $server->not_found_script = "not_found.php";

  if ($server->error !== false) {
    $res->send_code(400, array("message" => $server->error));
  }

  $server->add_route("/", "GET", "index.php");
  $server->add_route("/message", "GET", "message.php");

  // send a 404 error
  $server->not_found();
  